local sensorInfo = {
    name = "Return known enemy positions"
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function()
    enemies = Sensors.core.EnemyUnits()
    local result = {}
    for i = 1, #enemies do
        local enemyId = enemies[i]
        if (result[enemyId] == nil) then
            local x,y,z = Spring.GetUnitPosition(enemyId)
            result[enemyId] = Vec3(x,y,z)
        end
    end
    return result
end