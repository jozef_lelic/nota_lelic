local sensorInfo = {
    name = "Find path for a unit in a given nav mesh"
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end


return function(unitId, targetNode, navMesh, tileSize)
    local x, y, z = Spring.GetUnitPosition(unitId)
    local nx = math.floor(x / tileSize) * tileSize
    local nz = math.floor(z / tileSize) * tileSize
    local ny = Spring.GetGroundHeight(nx, nz)


    if (navMesh[nx][nz] == nil) then
        return nil
    end

    local path = {}

    local currentNode = Vec3(nx, ny, nz)
    while currentNode:Distance(targetNode) > (tileSize * 2) do
        table.insert(path, currentNode)
        currentNode = navMesh[currentNode.x][currentNode.z]
    end

    return path
end