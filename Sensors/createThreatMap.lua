local sensorInfo = {
    name = "Create threat map"
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function(enemyPositions, tileSize)
    local result = {}
    for x = 0, Game.mapSizeX, tileSize do
        result[x] = {}
        for z = 0, Game.mapSizeZ, tileSize do
            local tilePosition = Vec3(x, Spring.GetGroundHeight(x, z) + 20, z)
            local isSafe = true
            for id, position in pairs(enemyPositions) do
                if position:Distance(tilePosition) < 1000 then
                    isSafe = false
                    break
                end
            end
            result[x][z] = isSafe
        end
    end
    return result
end