local sensorInfo = {
    name = "Find units outside of safe area within specified distance"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function(safeSpot, safeSpotRadius, maxDistance)
    local result = {}
    allFriendlyUnits = Spring.GetTeamUnits(0)
    for i = 1, #allFriendlyUnits do
        local unitId = allFriendlyUnits[i]
        local unitName = UnitDefs[Spring.GetUnitDefID(unitId)].name
        local x, y, z = Spring.GetUnitPosition(unitId)
        local position = Vec3(x, y, z)
        local distanceToSafeArea = position:Distance(safeSpot)

        if (
                unitName ~= "armatlas"
                and unitName ~= "armrad"
                and unitName ~= "armwin"
                and distanceToSafeArea > safeSpotRadius
                and distanceToSafeArea < maxDistance
        ) then
            table.insert(result, unitId)
        end
    end
    return result
end