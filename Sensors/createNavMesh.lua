local sensorInfo = {
    name = "Create nav mesh",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

-- Queue https://www.lua.org/pil/11.4.html
function ListNew()
    return { first = 0, last = -1 }
end

List = {}
function List.new()
    return { first = 0, last = -1 }
end

function List.pushleft(list, value)
    local first = list.first - 1
    list.first = first
    list[first] = value
end

function List.pushright(list, value)
    local last = list.last + 1
    list.last = last
    list[last] = value
end

function List.popleft(list)
    local first = list.first
    if first > list.last then
        error("list is empty")
    end
    local value = list[first]
    list[first] = nil -- to allow garbage collection
    list.first = first + 1
    return value
end

function List.popright(list)
    local last = list.last
    if list.first > last then
        error("list is empty")
    end
    local value = list[last]
    list[last] = nil -- to allow garbage collection
    list.last = last - 1
    return value
end

function List.size(list)
    return list.last - list.first + 1
end

-- end Queue

return function(safeArea, threatMap, tileSize)

    local x = math.floor(safeArea.x / tileSize) * tileSize
    local z = math.floor(safeArea.z / tileSize) * tileSize
    local y = Spring.GetGroundHeight(x, z)

    local result = {}

    local queue = List.new()
    List.pushleft(queue, Vec3(x, y, z))

    while List.size(queue) > 0 do
        local current = List.popright(queue)

        local neighbours = {}
        for dx = -1, 1 do
            for dz = -1, 1 do
                if dx ~= 0 or dz ~= 0 then
                    local neighbour = current + (Vec3(dx, 0, dz) * tileSize)
                    neighbour.y = Spring.GetGroundHeight(neighbour.x, neighbour.z)

                    if neighbour.x >= 0 and neighbour.z >= 0
                            and neighbour.x <= Game.mapSizeX
                            and neighbour.z <= Game.mapSizeZ then
                        table.insert(neighbours, neighbour)
                    end
                end
            end
        end

        for i = 1, #neighbours do
            local neighbour = neighbours[i]
            local nx = neighbour.x
            local nz = neighbour.z

            if (threatMap[nx][nz]) then
                if (result[nx] == nil) then
                    result[nx] = {}
                end

                if (result[nx][nz] == nil) then
                    result[nx][nz] = current
                    List.pushleft(queue, neighbour)
                end
            end
        end
    end

    return result
end
