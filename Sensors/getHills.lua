local sensorInfo = {
    name = "Hills"
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function()
    local d = 128
    local h = 512
    local hills = {}
    for x=0, Game.mapSizeX, d do
        for z =0, Game.mapSizeX, d do
            height = Spring.GetGroundHeight(x,z)
            if (height > 190) then
                table.insert(hills, {x=x, z=z, y=height})
            end
        end
    end
    return hills
end