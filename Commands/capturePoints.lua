function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
        parameterDefs = {
            {
                name = "hills",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "",
            }
        }
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
	local hills = parameter.hills -- array of Vec3

    for i,pos in ipairs(hills) do
        local position = Vec3(pos.x, pos.y, pos.z)
        if (i>units.length) then
            return RUNNING
        end
        SpringGiveOrderToUnit(units[i], CMD.MOVE, position:AsSpringVector(), {})
    end

    return RUNNING
end


function Reset(self)
	ClearState(self)
end
