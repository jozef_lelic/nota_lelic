function getInfo()
    return {
        tooltip = "Transport target units to specified area",
        parameterDefs = {
            {
                name = "targetUnits",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "<none>",
            },
            {
                name = "targetArea",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "<none>",
            },
            {
                name = "targetAreaRadius",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "<none>",
            },

            {
                name = "navMesh",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "<none>",
            },
            {
                name = "tileSize",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "<none>",
            }
        }
    }
end

local GetCommands = Spring.GetUnitCommands
local GiveOrder = Spring.GiveOrderToUnit
local IsTransporting = Spring.GetUnitIsTransporting

local STATE_READY = "READY"
local STATE_PICK_UP = "PICKING_UP"
local STATE_TRANSPORT = "TRANSPORTING"
local STATE_DROP = "DROPPING"

local function findTarget(self, targetUnits)
    local index = self.processed + 1
    if index > #targetUnits then
        return nil
    end
    self.processed = index
    return targetUnits[index]
end

local function waitForFinish(unit)
    GiveOrder(unit, CMD.MOVE, Vec3(0, Spring.GetGroundHeight(0, 0)):AsSpringVector(), {})
    GiveOrder(unit, CMD.WAIT, {}, CMD.OPT_SHIFT)
end

local function pickUp(self, unit, parameter)
    local targetUnits = parameter.targetUnits

    local unitToLoad
    local path
    while not path do
        unitToLoad = findTarget(self, targetUnits)

        if unitToLoad == nil then
            waitForFinish(unit)
            return
        end

        path = Sensors.calculatePath(unitToLoad, parameter.targetArea, parameter.navMesh, parameter.tileSize)
        self.paths[unit] = path
    end

    for i = #path, 1, -1 do
        GiveOrder(unit, CMD.MOVE, path[i]:AsSpringVector(), CMD.OPT_SHIFT)
    end
    GiveOrder(unit, CMD.LOAD_UNITS, { unitToLoad }, CMD.OPT_SHIFT)

    self.states[unit] = STATE_PICK_UP
end

local function transport(self, unit, parameter)
    local path = self.paths[unit]

    for i = 1, #path do
        GiveOrder(unit, CMD.MOVE, path[i]:AsSpringVector(), CMD.OPT_SHIFT)
    end

    self.states[unit] = STATE_TRANSPORT
end

local function drop(self, unit, parameter)
    local temp = parameter.targetArea
    local targetArea = Vec3(temp.x, temp.y, temp.z)
    local targetAreaRadius = parameter.targetAreaRadius

    GiveOrder(unit, CMD.TIMEWAIT, { 200 }, CMD.OPT_SHIFT)
    GiveOrder(unit, CMD.UNLOAD_UNITS, { targetArea.x, targetArea.y, targetArea.z, targetAreaRadius }, CMD.OPT_SHIFT)

    self.states[unit] = STATE_DROP
end

local function ClearState(self)
    self.in_progress = true
    self.paths = {}
    self.states = {}
    self.processed = 0
end

function Run(self, units, parameter)
    if not self.in_progress then
        ClearState(self)
    end


    for i = 1, #units do
        local unit = units[i]

        if (self.states[unit] == nil) then
            self.states[unit] = STATE_READY
        end

        local state = self.states[unit];

        Spring.Echo(unit .. ": " .. self.states[unit])
        local cmds = GetCommands(unit, 1)
        if #cmds == 0 then
            if (state == STATE_READY) then
                pickUp(self, unit, parameter)

            elseif (state == STATE_PICK_UP) then
                transport(self, unit, parameter)

            elseif (state == STATE_TRANSPORT) then
                drop(self, unit, parameter)

            elseif (state == STATE_DROP) then
                if #IsTransporting(unit) > 0 then
                    self.states[unit] = STATE_TRANSPORT
                else
                    self.states[unit] = STATE_READY
                end
            end
        end
    end

    return RUNNING
end

function Reset(self)
    ClearState(self)
end
